package dto;

import java.sql.Date;

public class EmpDTO {
	private String empId;
	private String empName;
	private String mail;
	private Date birthday;
	private int salary;
	private String password;
	private String deptId;

	public String getempId() {
		return empId;
	}

	public void setempId(String empId) {
		this.empId = empId;
	}

	public String getempName() {
		return empName;
	}

	public void setempName(String empName) {
		this.empName = empName;
	}

	public String getmail() {
		return mail;
	}

	public void setmail(String mail) {
		this.mail = mail;
	}

	public Date getbirthday() {
		return birthday;
	}

	public void setbirthday(Date birthday) {
		this.birthday = birthday;
	}

	public int getsalary() {
		return salary;
	}

	public void setsalary(int salary) {
		this.salary = salary;
	}

	public String getpassword() {
		return password;
	}

	public void setpassword(String password) {
		this.password = password;
	}

	public String getdeptId() {
		return deptId;
	}

	public void setdeptId(String deptId) {
		this.deptId = deptId;
	}

}
