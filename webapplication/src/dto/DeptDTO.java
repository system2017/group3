package dto;

public class DeptDTO {
	private String deptId;
	private String deptName;

	public DeptDTO() {
	}

	public DeptDTO(String deptId, String deptName) {
		super();
		this.deptId = deptId;
		this.deptName = deptName;
	}

	public String getdeptId() {
		return deptId;
	}

	public void setdeptId(String deptId) {
		this.deptId = deptId;
	}

	public String getdeptName() {
		return deptName;
	}

	public void setdeptName(String deptName) {
		this.deptName = deptName;
	}

}
