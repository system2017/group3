package util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionManager {
	static {
		try {
			Class.forName("org.postgresql.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}
	private Connection connection;

	public Connection getConnection() {
		try {
			connection = DriverManager.getConnection("jdbc:postgresql:webapplication", "webapplication",
					"webapplication");
			connection.setAutoCommit(false);
			return connection;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	public void closeConnection() {
		if (connection != null) {
			try {
				connection.close();
			} catch (SQLException e) {
				throw new RuntimeException(e);
			}
		}
	}

	public void commitTransaction() {
		if (connection != null) {
			try {
				connection.commit();
			} catch (SQLException e) {
				throw new RuntimeException(e);
			}
		}
	}

	public void rollbackTransaction() {
		if (connection != null) {
			try {
				connection.rollback();
			} catch (SQLException e) {
				throw new RuntimeException(e);
			}
		}
	}
}
