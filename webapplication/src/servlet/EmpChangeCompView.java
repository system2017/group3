
package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.EmpDAO;
import dto.EmpDTO;

/**
 * Servlet implementation class EmpChangeCompView
 */
@WebServlet("/emp_change_complete")
public class EmpChangeCompView extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

		HttpSession session = request.getSession();
		EmpDTO empChange = (EmpDTO) session.getAttribute("empChange");

		EmpDAO empDAO = new EmpDAO();

		empDAO.change(empChange);

		session.removeAttribute("empChange");

		request.setAttribute("empChange", empChange);

		RequestDispatcher requestDispatcher = request.getRequestDispatcher("/WEB-INF/jsp/empChangeComplete.jsp");
		requestDispatcher.forward(request, response);

	}

}
