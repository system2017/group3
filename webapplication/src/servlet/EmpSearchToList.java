package servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.EmpDAO;
import dto.EmpDTO;

/**
 * Servlet implementation class EmpSearchToList
 */
@WebServlet("/emp_search_to_list")
public class EmpSearchToList extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String empId = request.getParameter("empId");
		String empName = request.getParameter("empName");
		String deptId = request.getParameter("deptId");

		EmpDAO empDAO = new EmpDAO();
		ArrayList<EmpDTO> list = empDAO.search(empId, empName, deptId);

		request.setAttribute("emplist", list);

		RequestDispatcher requestDispatcher = request.getRequestDispatcher("/WEB-INF/jsp/empList.jsp");
		requestDispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
