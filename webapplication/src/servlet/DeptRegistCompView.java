package servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.DeptDAO;
import dto.DeptDTO;

/**
 * Servlet implementation class DeptRegistCompView
 */
@WebServlet("/dept_regist_complete")
public class DeptRegistCompView extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		DeptDTO deptRegist = (DeptDTO) session.getAttribute("deptRegist");

		DeptDAO deptDAO = new DeptDAO();

		DeptDTO selectOne = deptDAO.selectOne(deptRegist.getdeptId());

		if (selectOne != null) {
			request.setAttribute("errorMessage", "IDが重複しています");

			RequestDispatcher requestDispatcher = request.getRequestDispatcher("/WEB-INF/jsp/deptRegist.jsp");
			requestDispatcher.forward(request, response);

		} else {
			deptDAO.insert(deptRegist);

			// session内の部署データ更新
			ArrayList<DeptDTO> list = deptDAO.selectAll();
			session.setAttribute("dept", list);
			// ----------------------------------

			session.removeAttribute("deptRegist");

			request.setAttribute("deptRegist", deptRegist);

			RequestDispatcher requestDispatcher = request.getRequestDispatcher("/WEB-INF/jsp/deptRegistComplete.jsp");
			requestDispatcher.forward(request, response);

		}

	}

}
