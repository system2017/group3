package servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DeptDAO;
import dto.DeptDTO;

/**
 * Servlet implementation class DeptSearchToList
 */
@WebServlet("/dept_search_to_list")
public class DeptSearchToList extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String deptId = request.getParameter("deptId");
		String deptName = request.getParameter("deptName");

		DeptDAO deptDAO = new DeptDAO();
		ArrayList<DeptDTO> list = deptDAO.search(deptId, deptName);

		request.setAttribute("deptlist", list);

		RequestDispatcher requestDispatcher = request.getRequestDispatcher("/WEB-INF/jsp/deptList.jsp");
		requestDispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
