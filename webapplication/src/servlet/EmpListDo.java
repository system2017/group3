package servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.EmpDAO;
import dto.EmpDTO;

/*Servlet implementation

class EmpListDo*/
@WebServlet("/emp_list")
public class EmpListDo extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		EmpDAO empDAO = new EmpDAO();
		ArrayList<EmpDTO> emplist = empDAO.selectAll();

		request.setAttribute("emplist", emplist);
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("/WEB-INF/jsp/empList.jsp");
		requestDispatcher.forward(request, response);

	}

}
