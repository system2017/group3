package servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.DeptDAO;
import dao.EmpDAO;
import dto.DeptDTO;
import dto.EmpDTO;

/**
 * Servlet implementation class LoginCheck
 */
@WebServlet("/menu")
public class LoginCheck extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String empId = request.getParameter("loginId");
		String password = request.getParameter("password");

		if (empId.isEmpty()) {
			String err = "IDが空です。";
			request.setAttribute("errormsg", err);
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
			requestDispatcher.forward(request, response);
			return;
		}

		if (password.isEmpty()) {
			String err = "パスワードが空です。";
			request.setAttribute("errormsg", err);
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
			requestDispatcher.forward(request, response);
			return;
		}
		EmpDAO empDAO = new EmpDAO();
		EmpDTO empDTO = empDAO.selectOne(empId);

		DeptDAO deptDAO = new DeptDAO();
		ArrayList<DeptDTO> list = deptDAO.selectAll();

		if (empDTO == null || !(password.equals(empDTO.getpassword()))) {
			String err = "IDかパスワードが間違っています。";
			request.setAttribute("errormsg", err);
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
			requestDispatcher.forward(request, response);
		} else {
			HttpSession session = request.getSession(true);
			session.setAttribute("dept", list);
			session.setAttribute("user", empDTO);
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("/WEB-INF/jsp/menu.jsp");
			requestDispatcher.forward(request, response);

		}
	}
}
