package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dto.DeptDTO;

/**
 * Servlet implementation class DeptChangeCheckView
 */
@WebServlet("/dept_change_check")
public class DeptChangeCheckView extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String deptName = request.getParameter("dept_name");

		if (deptName.isEmpty()) {
			String err = "空き項目があります。部署名は必須入力項目です。";
			request.setAttribute("errormsg", err);
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("/WEB-INF/jsp/deptChange.jsp");
			requestDispatcher.forward(request, response);
			return;
		}

		HttpSession session = request.getSession(true);

		DeptDTO deptDTO = (DeptDTO) session.getAttribute("deptDetail");
		DeptDTO deptDTO2 = new DeptDTO();

		deptDTO2.setdeptId(deptDTO.getdeptId());
		deptDTO2.setdeptName(deptName);

		session.setAttribute("deptChange", deptDTO2);
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("/WEB-INF/jsp/deptChangeCheck.jsp");
		requestDispatcher.forward(request, response);

	}

}
