package servlet;

import java.io.IOException;
import java.sql.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dto.EmpDTO;

/**
 * Servlet implementation class EmpChangeCheckView
 */
@WebServlet("/emp_change_check")
public class EmpChangeCheckView extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String empName = request.getParameter("emp_name");
		String mail = request.getParameter("mail");
		String birthday = request.getParameter("birthday");
		String salary = request.getParameter("salary");
		String deptId = request.getParameter("deptId");
		String oldpass = request.getParameter("passwordPrev");
		String newpass = request.getParameter("password");
		String newpass2 = request.getParameter("passwordConfirm");

		if (empName.isEmpty() || newpass.isEmpty() || newpass2.isEmpty()) {
			String err = "空き項目があります。社員名は必須入力項目です。";
			request.setAttribute("errormsg", err);
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("/WEB-INF/jsp/empChange.jsp");
			requestDispatcher.forward(request, response);
			return;
		}

		HttpSession session = request.getSession(true);
		EmpDTO empDTO = (EmpDTO) session.getAttribute("empDetail");

		if (!(empDTO.getpassword().equals(oldpass)) || !(newpass.equals(newpass2))) {
			String err = "パスワードが間違ってます。";
			request.setAttribute("errormsg", err);

			RequestDispatcher requestDispatcher = request.getRequestDispatcher("/WEB-INF/jsp/empChange.jsp");
			requestDispatcher.forward(request, response);
		} else {
			EmpDTO empDTO2 = new EmpDTO();
			empDTO2.setempId(empDTO.getempId());
			empDTO2.setempName(empName);
			empDTO2.setmail(mail);
			try {
				empDTO2.setbirthday(Date.valueOf(birthday));
				empDTO2.setsalary(Integer.parseInt(salary));
			} catch (Exception e) {
				String err = "入力形式が間違ってます。";
				request.setAttribute("errormsg", err);

				RequestDispatcher requestDispatcher = request.getRequestDispatcher("/WEB-INF/jsp/empChange.jsp");
				requestDispatcher.forward(request, response);
				return;
			}
			empDTO2.setdeptId(deptId);
			empDTO2.setpassword(newpass);

			session.setAttribute("empChange", empDTO2);
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("/WEB-INF/jsp/empChangeCheck.jsp");
			requestDispatcher.forward(request, response);

		}

	}

}
