package servlet;

import java.io.IOException;
import java.sql.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dto.EmpDTO;

/**
 * Servlet implementation class EmpRegistCheckView
 */
@WebServlet("/emp_regist_check")
public class EmpRegistCheckView extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

		String id = request.getParameter("emp_id");
		String name = request.getParameter("emp_name");
		String mail = request.getParameter("mail");
		String birthday = request.getParameter("birthday");
		String salary = request.getParameter("salary");
		String pass1 = request.getParameter("pass1");
		String pass2 = request.getParameter("pass2");
		String dept = request.getParameter("deptId");

		EmpDTO empDTO = new EmpDTO();
		try {
			empDTO.setempId(id);
			empDTO.setempName(name);
			empDTO.setmail(mail);
			if (!(birthday.equals(""))) {
				empDTO.setbirthday(Date.valueOf(birthday));
			}
			if (!(salary.equals(""))) {
				empDTO.setsalary(Integer.parseInt(salary));
			}
			empDTO.setpassword(pass1);
			empDTO.setdeptId(dept);
		} catch (Exception e) {
			// TODO 自動生成された catch ブロック
			request.setAttribute("errorMessage", "入力エラー");

			RequestDispatcher requestDispatcher = request.getRequestDispatcher("/WEB-INF/jsp/empRegist.jsp");
			requestDispatcher.forward(request, response);
			return;
		}

		HttpSession session = request.getSession();
		session.setAttribute("empRegist", empDTO);

		if (!(pass1.equals(pass2)) || id.isEmpty() || name.isEmpty() || pass1.isEmpty() || pass2.isEmpty()) {
			request.setAttribute("errorMessage", "入力エラー");

			RequestDispatcher requestDispatcher = request.getRequestDispatcher("/WEB-INF/jsp/empRegist.jsp");
			requestDispatcher.forward(request, response);
			return;
		} else {
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("/WEB-INF/jsp/empRegistCheck.jsp");
			requestDispatcher.forward(request, response);

		}
	}

}
