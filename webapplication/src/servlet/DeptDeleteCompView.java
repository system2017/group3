package servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.DeptDAO;
import dto.DeptDTO;

/**
 * Servlet implementation class DeptDeleteCompView
 */
@WebServlet("/dept_delete_complete")
public class DeptDeleteCompView extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		DeptDTO deptDelete = (DeptDTO) session.getAttribute("deptDetail");

		DeptDAO deptDAO = new DeptDAO();

		deptDAO.delete(deptDelete.getdeptId());

		// session内の部署データ更新
		ArrayList<DeptDTO> list = deptDAO.selectAll();
		session.setAttribute("dept", list);
		// ----------------------------------

		session.removeAttribute("deptDetail");

		request.setAttribute("deptDetail", deptDelete);

		RequestDispatcher requestDispatcher = request.getRequestDispatcher("/WEB-INF/jsp/deptDeleteComplete.jsp");
		requestDispatcher.forward(request, response);

	}

}
