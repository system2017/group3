package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dto.DeptDTO;

/**
 * Servlet implementation class EmpRegistCheckView
 */
@WebServlet("/dept_regist_check")
public class DeptRegistCheckView extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

		String id = request.getParameter("dept_id");
		String name = request.getParameter("dept_name");

		DeptDTO deptDTO = new DeptDTO();
		deptDTO.setdeptId(id);
		deptDTO.setdeptName(name);

		HttpSession session = request.getSession();
		session.setAttribute("deptRegist", deptDTO);

		if (id.isEmpty() || name.isEmpty()) {
			request.setAttribute("errorMessage", "入力エラー");

			RequestDispatcher requestDispatcher = request.getRequestDispatcher("/WEB-INF/jsp/deptRegist.jsp");
			requestDispatcher.forward(request, response);
			return;
		} else {
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("/WEB-INF/jsp/deptRegistCheck.jsp");
			requestDispatcher.forward(request, response);

		}
	}

}
