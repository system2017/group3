package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.EmpDAO;
import dto.EmpDTO;

/**
 * Servlet implementation class EmpDeleteCompView
 */
@WebServlet("/emp_delete_complete")
public class EmpDeleteCompView extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		EmpDTO empDelete = (EmpDTO) session.getAttribute("empDetail");

		EmpDAO empDAO = new EmpDAO();

		empDAO.delete(empDelete.getempId());

		session.removeAttribute("empDetail");

		request.setAttribute("empDetail", empDelete);

		RequestDispatcher requestDispatcher = request.getRequestDispatcher("/WEB-INF/jsp/empDeleteComplete.jsp");
		requestDispatcher.forward(request, response);

	}

}
