package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.EmpDAO;
import dto.EmpDTO;

/**
 * Servlet implementation class EmpDetailView
 */
@WebServlet("/emp_detail")
public class EmpDetailView extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String empId = request.getParameter("empId");
		EmpDAO empDAO = new EmpDAO();
		EmpDTO emp = empDAO.selectOne(empId);
		HttpSession session = request.getSession();
		session.setAttribute("empDetail", emp);

		RequestDispatcher requestDispatcher = request.getRequestDispatcher("/WEB-INF/jsp/empDetail.jsp");
		requestDispatcher.forward(request, response);
	}

}
