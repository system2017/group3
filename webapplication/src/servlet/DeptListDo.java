package servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DeptDAO;
import dto.DeptDTO;

/**
 * Servlet implementation class DeptListDo
 */
@WebServlet("/dept_list")
public class DeptListDo extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		DeptDAO deptDAO = new DeptDAO();
		ArrayList<DeptDTO> deptlist = deptDAO.selectAll();

		request.setAttribute("deptlist", deptlist);
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("/WEB-INF/jsp/deptList.jsp");
		requestDispatcher.forward(request, response);
	}

}
