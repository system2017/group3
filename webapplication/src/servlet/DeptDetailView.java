package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.DeptDAO;
import dto.DeptDTO;

/**
 * Servlet implementation class DeptDetailView
 */
@WebServlet("/dept_detail")
public class DeptDetailView extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String deptId = request.getParameter("deptId");
		DeptDAO deptDAO = new DeptDAO();
		DeptDTO dept = deptDAO.selectOne(deptId);
		HttpSession session = request.getSession();
		session.setAttribute("deptDetail", dept);

		RequestDispatcher requestDispatcher = request.getRequestDispatcher("/WEB-INF/jsp/deptDetail.jsp");
		requestDispatcher.forward(request, response);
	}

}
