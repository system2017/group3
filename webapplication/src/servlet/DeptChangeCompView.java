
package servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.DeptDAO;
import dto.DeptDTO;

/**
 * Servlet implementation class DeptChangeCompView
 */
@WebServlet("/dept_change_complete")
public class DeptChangeCompView extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

		HttpSession session = request.getSession();
		DeptDTO deptChange = (DeptDTO) session.getAttribute("deptChange");

		DeptDAO deptDAO = new DeptDAO();

		deptDAO.change(deptChange);

		// session内の部署データ更新
		ArrayList<DeptDTO> list = deptDAO.selectAll();
		session.setAttribute("dept", list);
		// ----------------------------------

		session.removeAttribute("deptChange");

		request.setAttribute("deptChange", deptChange);

		RequestDispatcher requestDispatcher = request.getRequestDispatcher("/WEB-INF/jsp/deptChangeComplete.jsp");
		requestDispatcher.forward(request, response);

	}

}
