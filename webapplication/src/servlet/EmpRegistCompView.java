package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.EmpDAO;
import dto.EmpDTO;

/**
 * Servlet implementation class EmpRegistCompView
 */
@WebServlet("/emp_regist_complete")
public class EmpRegistCompView extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		EmpDTO empRegist = (EmpDTO) session.getAttribute("empRegist");

		EmpDAO empDAO = new EmpDAO();

		EmpDTO selectOne = empDAO.selectOne(empRegist.getempId());

		if (selectOne != null) {
			request.setAttribute("errorMessage", "IDが重複しています");

			RequestDispatcher requestDispatcher = request.getRequestDispatcher("/WEB-INF/jsp/empRegist.jsp");
			requestDispatcher.forward(request, response);

		} else {
			empDAO.insert(empRegist);

			session.removeAttribute("empRegist");

			request.setAttribute("empRegist", empRegist);

			RequestDispatcher requestDispatcher = request.getRequestDispatcher("/WEB-INF/jsp/empRegistComplete.jsp");
			requestDispatcher.forward(request, response);

		}

	}

}
