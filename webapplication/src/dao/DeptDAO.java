package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import dto.DeptDTO;
import util.ConnectionManager;

public class DeptDAO {

	public ArrayList<DeptDTO> selectAll() {
		ConnectionManager connectionManager = new ConnectionManager();

		try {
			Connection connection = connectionManager.getConnection();

			StringBuffer sb = new StringBuffer();
			sb.append("SELECT");
			sb.append("        *");
			sb.append("    FROM");
			sb.append("        dept");

			PreparedStatement prepareStatement = connection.prepareStatement(sb.toString());

			ResultSet rs = prepareStatement.executeQuery();

			ArrayList<DeptDTO> list = new ArrayList<>();
			while (rs.next()) {
				String c1 = rs.getString("deptid");
				String c2 = rs.getString("deptname");

				DeptDTO dept = new DeptDTO();
				dept.setdeptId(c1);
				dept.setdeptName(c2);
				list.add(dept);
			}
			return list;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			connectionManager.closeConnection();
		}

	}

	public DeptDTO selectOne(String did) {
		ConnectionManager connectionManager = new ConnectionManager();

		try {
			Connection connection = connectionManager.getConnection();

			StringBuffer sb = new StringBuffer();
			sb.append("SELECT");
			sb.append("        *");
			sb.append("    FROM");
			sb.append("        dept");
			sb.append(" WHERE");
			sb.append(" deptid = ?;");

			PreparedStatement prepareStatement = connection.prepareStatement(sb.toString());
			prepareStatement.setString(1, did);

			ResultSet rs = prepareStatement.executeQuery();
			DeptDTO dept = null;
			while (rs.next()) {
				String c1 = rs.getString("deptid");
				String c2 = rs.getString("deptname");

				dept = new DeptDTO();
				dept.setdeptId(c1);
				dept.setdeptName(c2);

			}
			return dept;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			connectionManager.closeConnection();
		}

	}

	public int insert(DeptDTO dept) {
		ConnectionManager connectionManager = new ConnectionManager();

		try {
			Connection connection = connectionManager.getConnection();

			StringBuffer sb = new StringBuffer();
			sb.append("INSERT");
			sb.append("    INTO");
			sb.append("        dept(");
			sb.append("            deptid");
			sb.append("            ,deptname");
			sb.append("        )");
			sb.append("    VALUES");
			sb.append("        (");
			sb.append("            ?");
			sb.append("            ,?");
			sb.append("        );");

			PreparedStatement prepareStatement = connection.prepareStatement(sb.toString());
			prepareStatement.setString(1, dept.getdeptId());
			prepareStatement.setString(2, dept.getdeptName());
			int result = prepareStatement.executeUpdate();
			connectionManager.commitTransaction();
			return result;
		} catch (SQLException e) {
			connectionManager.rollbackTransaction();
			throw new RuntimeException(e);
		} finally {
			connectionManager.closeConnection();
		}

	}

	public int change(DeptDTO dept) {
		ConnectionManager connectionManager = new ConnectionManager();

		try {
			Connection connection = connectionManager.getConnection();

			StringBuffer sb = new StringBuffer();
			sb.append("UPDATE");
			sb.append("        dept");
			sb.append("    SET");
			sb.append("        deptid = ?");
			sb.append("        ,deptname = ?");
			sb.append("    WHERE");
			sb.append("        deptid = ?;");

			PreparedStatement prepareStatement = connection.prepareStatement(sb.toString());
			prepareStatement.setString(1, dept.getdeptId());
			prepareStatement.setString(2, dept.getdeptName());
			prepareStatement.setString(3, dept.getdeptId());
			int result = prepareStatement.executeUpdate();
			connectionManager.commitTransaction();
			return result;
		} catch (SQLException e) {
			connectionManager.rollbackTransaction();
			throw new RuntimeException(e);
		} finally {
			connectionManager.closeConnection();
		}

	}

	public int delete(String did) {
		ConnectionManager connectionManager = new ConnectionManager();

		try {
			Connection connection = connectionManager.getConnection();

			StringBuffer sb = new StringBuffer();
			sb.append("DELETE");
			sb.append("    FROM");
			sb.append("        dept");
			sb.append("    WHERE");
			sb.append("        deptid = ?;");

			PreparedStatement prepareStatement = connection.prepareStatement(sb.toString());
			prepareStatement.setString(1, did);
			int result = prepareStatement.executeUpdate();
			connectionManager.commitTransaction();
			return result;
		} catch (SQLException e) {
			connectionManager.rollbackTransaction();
			throw new RuntimeException(e);
		} finally {
			connectionManager.closeConnection();
		}

	}

	public ArrayList<DeptDTO> search(String did, String dname) {
		ConnectionManager connectionManager = new ConnectionManager();

		try {
			Connection connection = connectionManager.getConnection();

			StringBuffer sb = new StringBuffer();
			sb.append("SELECT");
			sb.append("        *");
			sb.append("    FROM");
			sb.append("        dept");
			sb.append(" WHERE");
			sb.append(" deptid LIKE ? AND deptname LIKE ?;");

			PreparedStatement prepareStatement = connection.prepareStatement(sb.toString());

			if (did.equals("")) {
				prepareStatement.setString(1, "%");
			} else {
				prepareStatement.setString(1, did);
			}
			if (dname.equals("")) {
				prepareStatement.setString(2, "%");
			} else {
				prepareStatement.setString(2, "%" + dname + "%");
			}
			ResultSet rs = prepareStatement.executeQuery();

			ArrayList<DeptDTO> list = new ArrayList<>();
			while (rs.next()) {
				String c1 = rs.getString("deptid");
				String c2 = rs.getString("deptname");

				DeptDTO dept = new DeptDTO();
				dept.setdeptId(c1);
				dept.setdeptName(c2);
				list.add(dept);
			}
			return list;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			connectionManager.closeConnection();
		}
	}
}
