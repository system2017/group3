package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import dto.EmpDTO;
import util.ConnectionManager;

public class EmpDAO {

	public ArrayList<EmpDTO> selectAll() {
		ConnectionManager connectionManager = new ConnectionManager();

		try {
			Connection connection = connectionManager.getConnection();

			StringBuffer sb = new StringBuffer();
			sb.append("SELECT");
			sb.append("        *");
			sb.append("    FROM");
			sb.append("        emp");

			PreparedStatement prepareStatement = connection.prepareStatement(sb.toString());

			ResultSet rs = prepareStatement.executeQuery();

			ArrayList<EmpDTO> list = new ArrayList<>();
			while (rs.next()) {
				String c1 = rs.getString("empid");
				String c2 = rs.getString("empname");
				String c3 = rs.getString("mail");
				Date c4 = rs.getDate("birthday");
				int c5 = rs.getInt("salary");
				String c6 = rs.getString("password");
				String c7 = rs.getString("deptid");

				EmpDTO emp = new EmpDTO();
				emp.setempId(c1);
				emp.setempName(c2);
				emp.setmail(c3);
				emp.setbirthday(c4);
				emp.setsalary(c5);
				emp.setpassword(c6);
				emp.setdeptId(c7);
				list.add(emp);
			}
			return list;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			connectionManager.closeConnection();
		}

	}

	public EmpDTO selectOne(String eid) {
		ConnectionManager connectionManager = new ConnectionManager();

		try {
			Connection connection = connectionManager.getConnection();

			StringBuffer sb = new StringBuffer();
			sb.append("SELECT");
			sb.append("        *");
			sb.append("    FROM");
			sb.append("        emp");
			sb.append(" WHERE");
			sb.append(" empid = ?;");

			PreparedStatement prepareStatement = connection.prepareStatement(sb.toString());
			prepareStatement.setString(1, eid);

			ResultSet rs = prepareStatement.executeQuery();
			System.out.println(rs);
			EmpDTO emp = null;
			while (rs.next()) {
				String c1 = rs.getString("empid");
				String c2 = rs.getString("empname");
				String c3 = rs.getString("mail");
				Date c4 = rs.getDate("birthday");
				int c5 = rs.getInt("salary");
				String c6 = rs.getString("password");
				String c7 = rs.getString("deptid");

				emp = new EmpDTO();
				emp.setempId(c1);
				emp.setempName(c2);
				emp.setmail(c3);
				emp.setbirthday(c4);
				emp.setsalary(c5);
				emp.setpassword(c6);
				emp.setdeptId(c7);

			}
			return emp;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			connectionManager.closeConnection();
		}

	}

	public int insert(EmpDTO emp) {
		ConnectionManager connectionManager = new ConnectionManager();

		try {
			Connection connection = connectionManager.getConnection();

			StringBuffer sb = new StringBuffer();
			sb.append("INSERT");
			sb.append("    INTO");
			sb.append("        emp(");
			sb.append("            empid");
			sb.append("            ,empname");
			sb.append("            ,mail");
			sb.append("            ,birthday");
			sb.append("            ,salary");
			sb.append("            ,password");
			sb.append("            ,deptId");
			sb.append("        )");
			sb.append("    VALUES");
			sb.append("        (");
			sb.append("            ?");
			sb.append("            ,?");
			sb.append("            ,?");
			sb.append("            ,?");
			sb.append("            ,?");
			sb.append("            ,?");
			sb.append("            ,?");
			sb.append("        );");

			PreparedStatement prepareStatement = connection.prepareStatement(sb.toString());
			prepareStatement.setString(1, emp.getempId());
			prepareStatement.setString(2, emp.getempName());
			prepareStatement.setString(3, emp.getmail());
			prepareStatement.setDate(4, emp.getbirthday());
			prepareStatement.setInt(5, emp.getsalary());
			prepareStatement.setString(6, emp.getpassword());
			prepareStatement.setString(7, emp.getdeptId());
			int result = prepareStatement.executeUpdate();
			connectionManager.commitTransaction();
			return result;
		} catch (SQLException e) {
			connectionManager.rollbackTransaction();
			throw new RuntimeException(e);
		} finally {
			connectionManager.closeConnection();
		}

	}

	public int change(EmpDTO emp) {
		ConnectionManager connectionManager = new ConnectionManager();

		try {
			Connection connection = connectionManager.getConnection();

			StringBuffer sb = new StringBuffer();
			sb.append("UPDATE");
			sb.append("        emp");
			sb.append("    SET");
			sb.append("        empname = ?");
			sb.append("        ,mail = ?");
			sb.append("        ,birthday = ?");
			sb.append("        ,salary = ?");
			sb.append("        ,password = ?");
			sb.append("        ,deptid = ?");
			sb.append("    WHERE");
			sb.append("        empid = ?;");

			PreparedStatement prepareStatement = connection.prepareStatement(sb.toString());
			prepareStatement.setString(1, emp.getempName());
			prepareStatement.setString(2, emp.getmail());
			prepareStatement.setDate(3, emp.getbirthday());
			prepareStatement.setInt(4, emp.getsalary());
			prepareStatement.setString(5, emp.getpassword());
			prepareStatement.setString(6, emp.getdeptId());
			prepareStatement.setString(7, emp.getempId());
			int result = prepareStatement.executeUpdate();
			connectionManager.commitTransaction();
			return result;
		} catch (SQLException e) {
			connectionManager.rollbackTransaction();
			throw new RuntimeException(e);
		} finally {
			connectionManager.closeConnection();
		}

	}

	public int delete(String eid) {
		ConnectionManager connectionManager = new ConnectionManager();

		try {
			Connection connection = connectionManager.getConnection();

			StringBuffer sb = new StringBuffer();
			sb.append("DELETE");
			sb.append("    FROM");
			sb.append("        emp");
			sb.append("    WHERE");
			sb.append("        empid = ?;");

			PreparedStatement prepareStatement = connection.prepareStatement(sb.toString());
			prepareStatement.setString(1, eid);
			int result = prepareStatement.executeUpdate();
			connectionManager.commitTransaction();
			return result;
		} catch (SQLException e) {
			connectionManager.rollbackTransaction();
			throw new RuntimeException(e);
		} finally {
			connectionManager.closeConnection();
		}

	}

	public ArrayList<EmpDTO> search(String eid, String ename, String did) {
		ConnectionManager connectionManager = new ConnectionManager();

		try {
			Connection connection = connectionManager.getConnection();

			StringBuffer sb = new StringBuffer();
			sb.append("SELECT");
			sb.append("        *");
			sb.append("    FROM");
			sb.append("        emp");
			sb.append(" WHERE");
			sb.append(" empid LIKE ? AND empname LIKE ? AND deptid LIKE ?;");

			PreparedStatement prepareStatement = connection.prepareStatement(sb.toString());

			if (eid.equals("")) {
				prepareStatement.setString(1, "%");
			} else {
				prepareStatement.setString(1, eid);
			}
			if (ename.equals("")) {
				prepareStatement.setString(2, "%");
			} else {
				prepareStatement.setString(2, "%" + ename + "%");
			}
			if (did.equals("")) {
				prepareStatement.setString(3, "%");
			} else {
				prepareStatement.setString(3, did);
			}
			ResultSet rs = prepareStatement.executeQuery();

			ArrayList<EmpDTO> list = new ArrayList<>();
			while (rs.next()) {
				String c1 = rs.getString("empid");
				String c2 = rs.getString("empname");
				String c3 = rs.getString("mail");
				Date c4 = rs.getDate("birthday");
				int c5 = rs.getInt("salary");
				String c6 = rs.getString("password");
				String c7 = rs.getString("deptid");

				EmpDTO emp = new EmpDTO();
				emp.setempId(c1);
				emp.setempName(c2);
				emp.setmail(c3);
				emp.setbirthday(c4);
				emp.setsalary(c5);
				emp.setpassword(c6);
				emp.setdeptId(c7);
				list.add(emp);
			}
			return list;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			connectionManager.closeConnection();
		}
	}
}
