<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>社員管理システム</title>
<link
	href="/webapplication/bootstrap/css/bootstrap.min.css"
	rel="stylesheet">
</head>
<body>
	<script
		src="/webapplication/bootstrap/js/bootstrap.min.js"></script>
	<h1>社員詳細</h1>

	<div class="container">
		<form class="form-horizontal">
			<div class="form-group">
				<label class="control-label col-xs-2">社員ID</label> <label
					class="control-label col-xs-2"><c:out value="${empDetail.empId }"></c:out></label>
			</div>
			<div class="form-group">
				<label class="control-label col-xs-2">社員名</label> <label
					class="control-label col-xs-2"><c:out value="${empDetail.empName }"></c:out></label>
			</div>
			<div class="form-group">
				<label class="control-label col-xs-2">メールアドレス</label> <label
					class="control-label col-xs-2"><c:out value="${empDetail.mail }"></c:out></label>
			</div>
			<div class="form-group">
				<label class="control-label col-xs-2">生年月日</label> <label
					class="control-label col-xs-2"><c:out value="${empDetail.birthday }"></c:out></label>
			</div>
			<div class="form-group">
				<label class="control-label col-xs-2">給与</label> <label
					class="control-label col-xs-2"><c:out value="${empDetail.salary }"></c:out></label>
			</div>
			<div class="form-group">
				<label class="control-label col-xs-2">部署</label> <label
					class="control-label col-xs-2"> <c:forEach items="${dept}"
						var="d">
						<c:if test="${empDetail.deptId == d.deptId}">
						<c:out value="${d.deptName}"></c:out>
					</c:if>
					</c:forEach>
				</label>
			</div>
		</form>
	</div>
	<br>
	<div class="col-xs-offset-1 col-xs-10">
		<form action="/webapplication/emp_change">
			<button type="submit" class="btn btn-primary">更新</button>
		</form>
		<br>
		<form action="/webapplication/emp_delete_check">
			<button type="submit" class="btn btn-danger">削除</button>
		</form>
	</div>
</body>
</html>