<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE >
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>社員管理システム</title>
<link
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
	rel="stylesheet">
</head>
<body background="/image/matrix-code-bg-2829.jpg">
	<script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<h1>部署登録</h1>
	<h2>以下の内容で登録しました</h2>

	<br>
	<div class="container">
		<form class="form-horizontal">
			<div class="form-group">
				<label class="control-label col-xs-1">部署ID</label> <label
					class="control-label col-xs-1"><c:out value="${deptRegist.deptId }"></c:out></label>
			</div>
			<div class="form-group">
				<label class="control-label col-xs-1">部署名</label> <label
					class="control-label col-xs-1"><c:out value="${deptRegist.deptName }"></c:out></label>
			</div>
		</form>
	</div>
	<br>
	<div class="col-xs-offset-1 col-xs-10">
		<form action="/webapplication/complete_to_menu">
			<button type="submit" class="btn btn-default">メニューへ</button>
		</form>
	</div>
</body>
</html>