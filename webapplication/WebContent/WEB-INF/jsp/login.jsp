<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>社員管理システム</title>
<link
	href="/webapplication/bootstrap/css/bootstrap.min.css"
	rel="stylesheet">
</head>
<body>
	<script
		src="/webapplication/bootstrap/js/bootstrap.min.js"></script>
	<div>
		<font size=7>社員管理システム</font>
	</div>
	<c:if test="${!empty errormsg}">
<div class="alert alert-danger" role="alert">${errormsg}</div>
</c:if>
	<br>
	<form action="/webapplication/menu" method="post">
		<div class="form-inline">
			<label for="loginId">ログインID:</label> <input required
				placeholder='login ID' type="text" name="loginId"
				class="form-control">
		</div>
		<br>
		<div class="form-inline">
			<label for="password">パスワード:</label> <input required
				placeholder='PASSWORD' type="text" name="password"
				class="form-control">
		</div>
		<br>
		<div class="col-xs-offset-0 col-xs-10">
			<button type="submit" class="btn btn-default">ログイン</button>
		</div>
	</form>
</body>
</html>