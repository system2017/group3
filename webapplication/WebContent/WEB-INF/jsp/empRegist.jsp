<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>社員管理システム</title>
<link href="/webapplication/bootstrap/css/bootstrap.min.css"
	rel="stylesheet">
</head>
<body>
	<script src="/webapplication/bootstrap/js/bootstrap.min.js"></script>
	<h1>社員登録</h1>
	<br>
	<c:if test="${!empty errormsg}">
<div class="alert alert-danger" role="alert">${errorMessage }</div>
</c:if>
	<div class="container">
		<form class="form-horizontal"
			action="/webapplication/emp_regist_check" method="post">
			<div class="form-group">
				<label class="control-label col-xs-2">社員ID(必須項目)</label>
				<div class="col-xs-5">
					<input type="number" min="1" name="emp_id"
						value="<c:out value="${empRegist.empId }"></c:out>" class="form-control">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-xs-2">社員名(必須項目)</label>
				<div class="col-xs-5">
					<input type="text" name="emp_name" value="<c:out value="${empRegist.empName }"></c:out>"
						class="form-control">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-xs-2">メールアドレス</label>
				<div class="col-xs-5">
					<input type="text" name="mail" value="<c:out value="${empRegist.mail }"></c:out>"
						class="form-control">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-xs-2">生年月日</label>
				<div class="col-xs-5">
					<input type="date" name="birthday" value="<c:out value="${empRegist.birthday }"></c:out>"
						class="form-control">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-xs-2">給与</label>
				<div class="col-xs-5">
					<input type="number" name="salary" value="<c:out value="${empRegist.salary }"></c:out>"
						class="form-control">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-xs-2">パスワード</label>
				<div class="col-xs-5">
					<input type="text" name="pass1" class="form-control">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-xs-2">パスワード(確認)</label>
				<div class="col-xs-5">
					<input type="text" name="pass2" class="form-control">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-xs-2">部署</label>

				<div class="col-xs-5">
					<select class="form-control" name="deptId">
						<option value="" selected>選択なし</option>
						<c:forEach items="${dept}" var="e">
							<option value="${e.deptId }"><c:out
									value="${e.deptName }"></c:out></option>
						</c:forEach>
					</select>
				</div>
			</div>
			<div class="form-group">
				<div class="col-xs-offset-2 col-xs-10">
					<button type="submit" class="btn btn-primary">確認</button>
				</div>
			</div>
		</form>
	</div>
</body>
</html>