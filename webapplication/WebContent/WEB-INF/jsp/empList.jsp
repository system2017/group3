<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>社員管理システム</title>
<link
	href="/webapplication/bootstrap/css/bootstrap.min.css"
	rel="stylesheet">
</head>
<body>
	<script
		src="/webapplication/bootstrap/js/bootstrap.min.js"></script>
	<h1>社員一覧</h1>
	<table border="1" class="table table-striped">
		<tr>
			<th>社員ID</th>
			<th>社員名</th>
			<th>メールアドレス</th>
			<th>生年月日</th>
			<th>部署名</th>
			<th></th>
		</tr>
		<c:forEach items="${emplist}" var="e">
			<tr>
				<td><c:out value="${e.empId}"></c:out></td>
				<td><c:out value="${e.empName}"></c:out></td>
				<td><c:out value="${e.mail}"></c:out></td>
				<td><c:out value="${e.birthday}"></c:out></td>
				<td><c:forEach items="${dept}" var="d">
					<c:if test="${e.deptId == d.deptId}">
						${d.deptName}
					</c:if>
				</c:forEach></td>
				<td><a href="/webapplication/emp_detail?empId=${e.empId}">詳細へ</a></td>
			</tr>
		</c:forEach>
	</table>
</body>
</html>