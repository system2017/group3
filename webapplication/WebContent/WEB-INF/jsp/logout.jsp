<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>社員管理システム</title>
<link href="/webapplication/bootstrap/css/bootstrap.min.css"
	rel="stylesheet">
</head>
<body>
	<script src="/webapplication/bootstrap/js/bootstrap.min.js"></script>
	<h1>ログアウト</h1>
	<h2>ログアウトしてもよろしいでしょうか？</h2>
	<br>
	<div class="col-xs-offset-0 col-xs-10">
		<form action="/webapplication/LogoutDo" method="post">
			<button type="submit" class="btn btn-primary">確定</button>
		</form>
		<br>
		<form action="/webapplication/complete_to_menu" method="post">
			<button type="submit" class="btn btn-default">戻る</button>
		</form>
	</div>
</body>
</html>