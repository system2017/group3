<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>社員管理システム</title>
<link href="/webapplication/bootstrap/css/bootstrap.min.css"
	rel="stylesheet">
</head>
<body>
	<script src="/webapplication/bootstrap/js/bootstrap.min.js"></script>
	<h1>部署更新</h1>
	${errormsg }
	<br>
	<div class="container">
		<form class="form-horizontal"
			action="/webapplication/dept_change_check" method="post">
			<div class="form-group">
				<label class="control-label col-xs-1">部署ID</label> <label
					class="control-label col-xs-1"><c:out
						value="${deptDetail.deptId}"></c:out></label>
			</div>
			<div class="form-group">
				<label class="control-label col-xs-1">部署名</label>
				<div class="col-xs-5">
					<input type="text" name="dept_name" value="${deptDetail.deptName }"
						class="form-control">
				</div>
			</div>
			<div class="form-group">
				<div class="col-xs-offset-1 col-xs-10">
					<button type="submit" class="btn btn-primary">確認</button>
				</div>
			</div>
		</form>
	</div>

	<table border="1">
		<tr>
			<th>部署ID</th>
			<td><c:out value="${deptDetail.deptId}"></c:out></td>
		</tr>
		<tr>
			<th>部署名(必須項目)</th>
			<td><input required type="text" name="dept_name"
				value="${deptDetail.deptName }"></td>
		</tr>
	</table>
	<p>
		<input type="submit" value="確認">
	</p>
</body>
</html>