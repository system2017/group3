<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>社員管理システム</title>
<title>社員管理システム</title>
<link
	href="/webapplication/bootstrap/css/bootstrap.min.css"
	rel="stylesheet">
</head>
<body>
	<script
		src="/webapplication/bootstrap/js/bootstrap.min.js"></script>
	<h1>社員検索</h1>
	<br>
	<div class="container">
		<form class="form-horizontal"
			action="/webapplication/emp_search_to_list" method="post">
			<div class="form-group">
				<label class="control-label col-xs-1">社員ID</label>
				<div class="col-xs-5">
					<input type="text" name="empId"
						class="form-control">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-xs-1">社員名</label>
				<div class="col-xs-5">
					<input type="text" name="empName" class="form-control">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-xs-1">部署</label>

				<div class="col-xs-5">
					<select class="form-control" name="deptId">
						<option value="" selected>選択なし</option>
						<c:forEach items="${dept}" var="e">
							<option value="${e.deptId }"><c:out
									value="${e.deptName }"></c:out></option>
						</c:forEach>
					</select>
				</div>
			</div>
			<div class="form-group">
				<div class="col-xs-offset-1 col-xs-10">
					<button type="submit" class="btn btn-primary">確認</button>
				</div>
			</div>
		</form>
	</div>
</body>
</html>