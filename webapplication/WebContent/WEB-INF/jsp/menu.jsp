<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>社員管理システム</title>
  <link
	href="/webapplication/bootstrap/css/bootstrap.min.css"
	rel="stylesheet">
	<link
	href="/webapplication/font-awesome/css/font-awesome.min.css"
	rel="stylesheet">
  <link href="/webapplication/font-awesome/css/a.css" rel="stylesheet">
</head>
<body>
<script
		src="/webapplication/bootstrap/js/bootstrap.min.js"></script>
 <br>
 <div class="row">
 <h1 class="col-md-6">&nbsp;&nbsp;&nbsp;社員情報管理システム</h1>
 <h2 class="col-md-6 text-right"><c:out value="${user.empName}"></c:out>&nbsp;&nbsp;&nbsp;</h2>
 </div>
<br>

 <div class="well">
  <i class="fa fa-bars fa-3x"> メニュー</i><br><br>

<!-- <c:if test="${!empty errormsg}">
<div class="alert alert-danger" role="alert">${errormsg}</div>
</c:if> -->
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-7">
        <div class="panel panel-success">
          <div class="panel-heading">
            <h6 class="panel-title">社員情報</h6>
          </div>
          <div class="panel-body">
          <form action="/webapplication/emp_regist"  style="display: inline">
            <button type="submit" class="btn-lg btn-info">&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-user fa-5x"></i>&nbsp;&nbsp;&nbsp;&nbsp;<br><br>社員登録</button>
            </form>
            &nbsp;&nbsp;&nbsp;
            <form action="/webapplication/emp_list" style="display: inline">
            <button type="submit" class="btn-lg btn-info">&nbsp;&nbsp;<i class="fa fa-list-alt fa-5x"></i>&nbsp;&nbsp;<br><br>社員一覧</button>
            </form>
            &nbsp;&nbsp;&nbsp;
            <form action="/webapplication/emp_search" style="display: inline">
            <button type="submit" class="btn-lg btn-info">&nbsp;&nbsp;&nbsp;<i class="fa fa-search fa-5x"></i>&nbsp;&nbsp;<br><br>社員検索</button>
          </form>
          </div>
        </div>
      </div>
      <div class="col-sm-3">
        <div class="panel panel-success">
          <div class="panel-heading">
            <h6 class="panel-title">その他</h6>
          </div>
          <div class="panel-body">
          <form action="/webapplication/logout">
            <button type="submit" class="btn-lg btn-info">&nbsp;&nbsp;&nbsp;<i class="fa fa-sign-out  fa-5x"></i>&nbsp;&nbsp;&nbsp;<br><br>ログアウト</button>
          </form>
          </div>
        </div>
      </div>

      <div class="col-sm-7">
        <div class="panel panel-success">
          <div class="panel-heading">
            <h6 class="panel-title">部署情報</h6>
          </div>
          <div class="panel-body">
          <form action="/webapplication/dept_regist" style="display: inline">
            <button type="submit" class="btn-lg btn-info">&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-building fa-5x"></i>&nbsp;&nbsp;&nbsp;<br><br>部署登録</button>
          </form>
            &nbsp;&nbsp;&nbsp;
            <form action="/webapplication/dept_list" style="display: inline">
            <button type="submit" class="btn-lg btn-info">&nbsp;&nbsp;<i class="fa fa-list-alt fa-5x"></i>&nbsp;&nbsp;<br><br>部署一覧</button>
          </form>
            &nbsp;&nbsp;&nbsp;
           <form action="/webapplication/dept_search" style="display: inline">
            <button type="submit" class="btn-lg btn-info">&nbsp;&nbsp;&nbsp;<i class="fa fa-search fa-5x"></i>&nbsp;&nbsp;<br><br>部署検索</button>
          </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>

<!-- <!-- <div class="panel panel-default"> -->
<!--   <div class="panel-body"> -->
<!--     パネルの内容 -->
<!--   </div> -->
<!-- </div> -->

<!-- <br> -->
<!-- <a class="icon fa-twitter" href="#">Twitter</a> -->
<!-- <br> -->
<!-- <a class="icon fa-facebook-official" href="#">Facebook</a> -->
<!-- <br> -->
<!-- <button type="button" class="btn btn-primary"><i class="icon fa-check"></i>確認</button> -->
<!-- <div style="padding:10px 0"> -->
<!--     <button type="button" class="btn btn-inverse"><i class="glyphicon glyphicon-ok"></i> OK</button> -->
<!--     <button type="button" class="btn btn-default"><i class="glyphicon glyphicon-search"></i> 検索</button> -->
<!--     <button type="button" class="btn-lg btn-info"><i class="fa fa-search fa-5x"></i><br><br>      拡大      </button> -->
<!--     <button type="button" class="btn btn-primary"><i class="glyphicon glyphicon-cog"></i> 設定</button> -->
<!--     <button type="button" class="btn btn-primary"><i class="glyphicon glyphicon-print"></i> 印刷</button> -->
<!-- </div> -->


<!--         <button class="btn btn-default"> -->
<!--             <span class="glyphicon glyphicon-search"></span>Search -->
<!--           </button> --> -->
<!-- </body> -->
<!-- </html> -->

<!-- 	<h1>メニュー</h1> -->
<%-- 	<p>ようこそ<c:out value="${user.empName}"></c:out>さん</p> --%>
<!-- 	<ul> -->
<!-- 		<li><a href="/webapplication/emp_regist">社員登録</a></li> -->
<!-- 		<li><a href="/webapplication/emp_list">社員一覧</a></li> -->
<!-- 		<li><a href="/webapplication/emp_search">社員検索</a></li> -->
<!-- 		<li><a href="/webapplication/dept_regist">部署登録</a></li> -->
<!-- 		<li><a href="/webapplication/dept_list">部署一覧</a></li> -->
<!-- 		<li><a href="/webapplication/dept_search">部署検索</a></li> -->
<!-- 		<li><a href="/webapplication/logout">ログアウト</a></li> -->
<!-- 	</ul> -->

<!-- </body> -->
<!-- </html> -->