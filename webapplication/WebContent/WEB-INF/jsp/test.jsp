<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>社員管理システム</title>
<link
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
	rel="stylesheet">
</head>
<body>
	<script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<div>
		<font size=7>社員管理システム</font>
	</div>
	${errormsg }
	<br>
	<form action="/webapplication/menu" method="post">
		<div class="form-inline">
			<label for="loginId">ログインID:</label> <input required placeholder='login ID' type="text" name="loginId"
				class="form-control">
		</div><br>
		<div class="form-inline">
			<label for="password">パスワード:</label> <input required placeholder='PASSWORD' type="text" name="password"
				class="form-control">
		</div><br>
		<input type="submit" value="ログイン">
	</form>
</body>
</html>