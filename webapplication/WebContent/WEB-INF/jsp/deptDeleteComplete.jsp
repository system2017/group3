<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>部署管理システム</title>
<link href="/webapplication/bootstrap/css/bootstrap.min.css"
	rel="stylesheet">
</head>
<body>
	<script src="/webapplication/bootstrap/js/bootstrap.min.js"></script>
	<div>
		<font size=7>部署削除</font><br> <font size=5>以下の部署を消しました。</font><br>
	</div>
	<br>

	<div class="container">
		<form class="form-horizontal">
			<div class="form-group">
				<label class="control-label col-xs-1">部署ID</label> <label
					class="control-label col-xs-1"><c:out value="${deptDetail.deptId }"></c:out></label>
			</div>
			<div class="form-group">
				<label class="control-label col-xs-1">部署名</label> <label
					class="control-label col-xs-1"><c:out value="${deptDetail.deptName }"></c:out></label>
			</div>
		</form>
	</div>
	<br>
	<div class="col-xs-offset-1 col-xs-10">
		<form action="/webapplication/complete_to_menu">
			<button type="submit" class="btn btn-default">メニューへ</button>
		</form>
	</div>

</body>
</html>