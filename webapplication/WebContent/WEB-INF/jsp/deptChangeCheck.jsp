<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>社員管理システム</title>
<link href="/webapplication/bootstrap/css/bootstrap.min.css"
	rel="stylesheet">
</head>
<body>
	<script src="/webapplication/bootstrap/js/bootstrap.min.js"></script>
	<h1>部署更新</h1>
	<h2>以下の内容を更新してよろしいでしょうか？</h2>

	<br>
		<div class="container">
		<form class="form-horizontal">
			<div class="form-group">
				<label class="control-label col-xs-1">部署ID</label> <label
					class="control-label col-xs-1"><c:out value="${deptChange.deptId }"></c:out></label>
			</div>
			<div class="form-group">
				<label class="control-label col-xs-1">部署名</label> <label
					class="control-label col-xs-1"><c:out value="${deptChange.deptName }"></c:out></label>
			</div>
		</form>
	</div>
	<br>
	<div class="col-xs-offset-1 col-xs-10">
		<form action="/webapplication/dept_change_complete" method="post">
			<button type="submit" class="btn btn-primary">確定</button>
		</form>
		<br>
		<form action="/webapplication/dept_back_to_change" method="post">
			<button type="submit" class="btn btn-default">戻る</button>
		</form>
	</div>
</body>
</html>