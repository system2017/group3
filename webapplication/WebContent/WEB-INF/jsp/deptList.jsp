<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>社員管理システム</title>
<link href="/webapplication/bootstrap/css/bootstrap.min.css"
	rel="stylesheet">
</head>
<body>
	<script src="/webapplication/bootstrap/js/bootstrap.min.js"></script>
	<h1>部署一覧</h1>

	<table border="1" class="table table-striped">
		<tr>
			<th>部署ID</th>
			<th>部署名</th>
			<th></th>
		</tr>
		<c:forEach items="${deptlist}" var="e">
			<tr>
				<td><c:out value="${e.deptId}"></c:out></td>
				<td><c:out value="${e.deptName}"></c:out></td>
				<td><a href="/webapplication/dept_detail?deptId=${e.deptId}">詳細へ</a></td>

			</tr>
		</c:forEach>
	</table>
</body>
</html>