<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>社員管理システム</title>
<link
	href="/webapplication/bootstrap/css/bootstrap.min.css"
	rel="stylesheet">
</head>
<body>
	<script
		src="/webapplication/bootstrap/js/bootstrap.min.js"></script>
	<h1>部署検索</h1>

	<br>
	<div class="container">
		<form class="form-horizontal"
			action="/webapplication/dept_search_to_list" method="post">
			<div class="form-group">
				<label class="control-label col-xs-1">部署ID</label>
				<div class="col-xs-5">
					<input type="text" name="deptId"
						class="form-control">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-xs-1">部署名</label>
				<div class="col-xs-5">
					<input type="text" name="deptName"
						class="form-control">
				</div>
			</div>
			<div class="form-group">
				<div class="col-xs-offset-1 col-xs-10">
					<button type="submit" class="btn btn-primary">検索</button>
				</div>
			</div>
		</form>
	</div>
</body>
</html>