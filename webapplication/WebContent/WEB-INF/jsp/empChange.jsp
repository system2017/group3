<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>社員管理システム</title>
<link href="/webapplication/bootstrap/css/bootstrap.min.css"
	rel="stylesheet">
</head>
<body>
	<script src="/webapplication/bootstrap/js/bootstrap.min.js"></script>
	<h1>社員更新</h1>
	${errormsg }

	<div class="container">
		<form class="form-horizontal"
			action="/webapplication/emp_change_check" method="post">
			<div class="form-group">
				<label class="control-label col-xs-2">社員ID</label>
				<label class="control-label col-xs-1"><c:out value="${empDetail.empId}"></c:out></label>
			</div>
			<div class="form-group">
				<label class="control-label col-xs-2">社員名</label>
				<div class="col-xs-5">
					<input type="text" name="emp_name" value="${empDetail.empName }"
						class="form-control">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-xs-2">メールアドレス</label>
				<div class="col-xs-5">
					<input type="text" name="mail" value="${empDetail.mail }"
						class="form-control">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-xs-2">生年月日</label>
				<div class="col-xs-5">
					<input type="date" name="birthday" value="${empDetail.birthday }"
						class="form-control">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-xs-2">給与</label>
				<div class="col-xs-5">
					<input type="number" name="salary" value="${empDetail.salary }"
						class="form-control">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-xs-2">前パスワード</label>
				<div class="col-xs-5">
					<input type="text" name="passwordPrev" class="form-control">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-xs-2">新パスワード</label>
				<div class="col-xs-5">
					<input type="text" name="password" class="form-control">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-xs-2">新パスワード(確認)</label>
				<div class="col-xs-5">
					<input type="text" name="passwordConfirm" class="form-control">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-xs-2">部署</label>

				<div class="col-xs-5">
					<select class="form-control" name="deptId">
						<c:forEach items="${dept}" var="e">
							<option value="${e.deptId }"><c:out
									value="${e.deptName }"></c:out></option>
						</c:forEach>
					</select>
				</div>
			</div>
			<div class="form-group">
				<div class="col-xs-offset-2 col-xs-10">
					<button type="submit" class="btn btn-primary">確認</button>
				</div>
			</div>
		</form>
	</div>
</body>
</html>